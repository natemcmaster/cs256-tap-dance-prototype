bbq.controller('ConnectedCtrl',function($scope){
    $scope.share = false;
    $scope.name = '';
    $scope.firstname = '';
    $scope.thumbnail = '';

    $scope.$rootScope.$watch('selectedPerson',function(value){
        if(value < 0 || value >= $scope.$rootScope.people.length){
            return;
        }

        var p = $scope.$rootScope.people[value];
        $scope.name = p.name;
        $scope.firstname = p.name.split(" ")[0];
        $scope.thumbnail = p.thumbnail;
        $scope.share = p.can_view;
    });

    function loadData(p) {
        for (var i = 0; i < $scope.$rootScope.people.length; i++) {
            if ($scope.$rootScope.people[i].name == p.name){
                $scope.$rootScope.nfcConnected=true;
                return;
            }
        }
        p.schedule = p.schedule.map(function(obj) {
            obj.time = new Date(obj.time);
            return obj;
        });
        $scope.$rootScope.nfcConnected = false;
        $scope.$rootScope.people.push(p);
        $scope.$rootScope.selectedPerson = $scope.$rootScope.people.length - 1;
        $scope.name = p.name;
        $scope.firstname = p.name.split(' ')[0];
        $scope.thumbnail = p.thumbnail;
        $scope.share = p.can_view;
        $scope.lookup('showView')('connected');
    }

    $scope.$rootScope.$watch('nfcConnected', function(value) {
        if (!value)
            return;
        var xhr = new XMLHttpRequest();
        xhr.onload = function() {
            loadData(JSON.parse(this.responseText));
        };
        xhr.open('GET', '/xhr/random_person', true);
        xhr.send();

    });

    $scope.toggleShare = function() {
        $scope.share = !$scope.share;
        $scope.$rootScope.toggleShareMySchedule();
    };
});
