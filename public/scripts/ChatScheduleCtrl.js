bbq.controller('ChatScheduleCtrl',function($scope){

	///////////////
	// INITIALIZE
	///////////////

	$scope.days = [];
	$scope.times = [];
	$scope.cells = [];
	$scope.selectedCell = null;

	$scope.$rootScope.$watchCalendar(function () {
		$scope.updateChatSchedule($scope.$rootScope.selectedPerson);
	});

	$scope.$rootScope.$watch('selectedPerson', function (value) {
		$scope.updateChatSchedule(value);
	});

	$scope.updateChatSchedule = function (selectedPerson) {
		bbq.Schedule.buildCalendar($scope);
		bbq.Schedule.addPersonMatches($scope, selectedPerson);
	};

	$scope.selectMatch = function (cell) {

		//change the old cell to inactive
		if ($scope.selectedCell !== null) {
			$scope.selectedCell.showActiveMatch = false;
			$scope.selectedCell.showInactiveMatch = true;
		}

		//change the current cell to active
		$scope.selectedCell = cell;
		$scope.selectedCell.showActiveMatch = true;
		$scope.selectedCell.showInactiveMatch = false;

		console.log("cell:", $scope.selectedCell);

		//Call the chat controller
		$scope.lookup('setInviteDate')(cell.date);
	};

});
