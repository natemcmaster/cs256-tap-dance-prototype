bbq.controller('MyScheduleCtrl',function($scope){

	///////////////
	// INITIALIZE
	///////////////

	$scope.days = [];
	$scope.times = [];
	$scope.cells = [];

	$scope.$rootScope.$watchCalendar(function () {
		bbq.Schedule.buildCalendar($scope);
		bbq.Schedule.addAvailables($scope);
	});
	
	$scope.toggleAvailable = function(cell){
		//Update the view
		cell.showAvailable = !cell.showAvailable;
		cell.showEmpty = !cell.showEmpty;
	};

});