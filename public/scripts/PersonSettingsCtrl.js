bbq.controller('PersonSettingsCtrl',function($scope){
    $scope.share = false;

    $scope.$rootScope.$watch('selectedPerson',function(value){
        if(value < 0 || value >= $scope.$rootScope.people.length){
            return;
        }

        var p = $scope.$rootScope.people[value];
        $scope.name = p.name;
        $scope.firstname = p.name.split(" ")[0];
        $scope.thumbnail = p.thumbnail;
        $scope.share = p.can_view;
    });

    $scope.toggleShare = function() {
        $scope.share = !$scope.share;
        $scope.$rootScope.toggleShareMySchedule();
    };

    $scope.deleteContact = function() {
        var idx = +$scope.$rootScope.selectedPerson;
        $scope.$rootScope.selectedPerson = -1;
        $scope.lookup('showView')('people');
        $scope.$rootScope.people.splice(idx, 1);
    };
});
