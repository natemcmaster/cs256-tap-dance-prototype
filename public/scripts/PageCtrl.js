bbq.controller('PageCtrl',function($scope){

    $scope.show={people:true};
    $scope.headerShow=true;

    $scope.tab = { people: true, schedule: false,connect: false};


    var pageTabMapping={
        people:'people',
        chat: 'people',
        person_settings:'people',
        connect:'connect',
        connected:'connect',
        schedule:'schedule',
        matches: 'matches'
    };

    $scope.$rootScope.showPerson = function(name){
        for(var x in $scope.$rootScope.people) {
            if($scope.$rootScope.people[x].name == name){
                $scope.$rootScope.people[x].new_messages = 0;
                $scope.$rootScope.selectedPerson = x;
                break;
            }
        }
        $scope.showView('chat');
    }

    $scope.$rootScope.showMatches = function(){        
        $scope.showView('matches');
    }

    $scope.showView = function(viewName) {
        for(var x in $scope.show) {
            var val=(x == viewName);
            $scope.show[x] = val;
            if(val)
                $scope.$rootScope.currentView = viewName;
        }
        var tab=pageTabMapping[viewName];
        for(x in $scope.tab)
            $scope.tab[x] = (x == tab);

        // special case -- auto update connect tab
        if(viewName == 'connect')
        {
            setTimeout(function(){
                if($scope.show.connect) {
                    $scope.$rootScope.nfcConnected=true;
                }
            },2*1000);
        }

    };

});
