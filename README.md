TapDance
-----
This is a prototype of a mobile app. This was created as a group project for BYU CS 256, "Understanding the User Experience". The purpose of this class was to learn basic design skills, how to develop UI, gather user feedback, and understand how users interact with software.


## Setup
This project requires Node.JS. Once these are installed and available on the command-line, 
the following commands will install this projects' dependencies.


`npm install`

## Launch
Run the server by executing `./bin/www`.

Run in DEBUG mode by executing `DEBUG=tap-that ./bin/www`

Access the site at [http://localhost:4000](http://localhost:4000)

To preview as a mobile app, go to [http://localhost:4000/preview.html](http://localhost:4000/preview.html)


## Preview
![Thumbnail screenshot](http://i.imgur.com/wpxUmMcl.png)
