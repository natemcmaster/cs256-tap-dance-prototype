(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var util = require('./util');

function ObservableArray(base) {
    base = base || [];
    this.handlers = {};
    this.push.apply(this, base);
    return this;
}
ObservableArray.prototype = Object.create(Array.prototype);
ObservableArray.prototype.constructor = ObservableArray;

ObservableArray.prototype.on = function(eventName, handler) {
    if (!this.handlers[eventName])
        this.handlers[eventName] = [];
    this.handlers[eventName].push(handler);
};

ObservableArray.prototype.trigger = function(eventName) {
    if (!this.handlers[eventName])
        return;
    for (var i = 0; i < this.handlers[eventName].length; i++) {
        var func = this.handlers[eventName][i];
        if (util.isFunction(func))
            func.apply(this, Array.prototype.slice.call(arguments, 1));
    }
};

ObservableArray.prototype.push = function() {
    var start = this.length;
    Array.prototype.push.apply(this, arguments);
    for (var i = 0; i < arguments.length; i++) {
        this.trigger('push', arguments[i], start + i);
    }
};

ObservableArray.prototype.splice = function(start,howMany){
    var originalLength=this.length;
    var ret=[];
    howMany = howMany || this.length;
    for(var i = 0; i < howMany; i++){
        ret.push(this[start+i]);
        delete this[start+i];
        this.length--;
    }
    for(var j = start+howMany, i=0; j < originalLength; j++, i++){
        this[start+i] = this[j];
        delete this[j];
    }

    this.trigger('splice',arguments[0], howMany);
    return ret;
};

module.exports = ObservableArray;

},{"./util":14}],2:[function(require,module,exports){
module.exports = function(bbq)
{
    bbq.directive('class-active-if',function(){
        return {
            link: function(element, $scope, attr)
            {
                var inverted= false;
                if(attr.substr) {
                    inverted = attr.substr(0,1)==='!';
                    if(inverted)
                        attr = attr.substr(1);
                }
                function setClass(src){
                    src = inverted ? !src : !!src;
                    if(src)
                        element.className+=' active';
                    else
                        element.className = element.className.replace(/active/g,'');
                }
                setClass($scope.lookup(attr));
                $scope.$watch(attr,function(value){
                    setClass(value);
                });
            }
        };
    });
};

},{}],3:[function(require,module,exports){
var util = require('../util');

module.exports = function(bbq) {
    bbq.directive('data', function() {
        return {
            link: function(element, $scope, attr) {
                var funcExpr = util.parseEventCall(attr);
                if (!funcExpr) {
                    util.bindAttribute(element, 'textContent', $scope, attr);
                    return;
                }
                var func = $scope.lookup(funcExpr.name);
                if (!util.isFunction(func)) {
                    return console.error('Could not find function "' + attr + '" on $scope');
                }
                var obj = {
                    result: ''
                };
                util.addWatcher(obj, 'result', function(val, trigger) {
                    util.setAttribute(element, 'textContent', val, trigger);
                });
                var update = function(name, val) {
                    var args = funcExpr.args.map(function(arg) {
                        if (arg.type == 'constant')
                            return arg.value;
                        if (arg.type == 'variable'){
                            return (arg.value == name) ? val : $scope.lookup(arg.value);
                        }
                        if (arg.type == '$event')
                            return event;
                    });
                    obj.result = func.apply(element, args);
                };
                for (var a in funcExpr.args) {
                    var arg = funcExpr.args[a];
                    if (arg.type == 'variable') {
                        $scope.$watch(arg.value, function(value) {
                            update(arg.value,value);
                        });
                    }
                }

                update();


            }
        };
    });
};

},{"../util":14}],4:[function(require,module,exports){
module.exports = function(bbq) {
    bbq.directive('disabled', function() {
        return {
            link: function(element, $scope, attr) {
                var inverted= false;
                if(attr.substr) {
                    inverted = attr.substr(0,1)==='!';
                    if(inverted)
                        attr = attr.substr(1);
                }
                var disable = function(val) {
                    val = inverted ? !val : !!val;
                    if (val)
                        element.setAttribute('disabled', 'disabled');
                    else
                        element.removeAttribute('disabled');
                };
                disable($scope.lookup(attr));
                $scope.$watch(attr, disable);
            }
        };
    });
};

},{}],5:[function(require,module,exports){
var util=require('../util');

function addEventHandler(element,$scope,attr, eventName){
    var eventCall = util.parseEventCall(attr,function(arg){
        if(arg=='$event')
            return {
                type:'$event'
            };
    });
    var func = (eventCall) ? $scope.lookup(eventCall.name) : null; // null safe
    if (util.isFunction(func)) {
        element.addEventListener(eventName, function(event){
            var args = eventCall.args.map(function(arg){
                if(arg.type=='constant')
                    return arg.value;
                if(arg.type=='variable')
                    return $scope.lookup(arg.value);
                if(arg.type=='$event')
                    return event;
            });
            func.apply(element,args);
        }, false);
    } else {
        console.error('Could not find function "'+eventCall.name+'" on $scope or $rootScope');
    }

}

module.exports = function(bbq) {
    ['click','submit','scroll','focus','blur','keyup'].forEach(function(e){
        bbq.directive(e, function() {
            return {
                link: function(element, $scope, attr) {
                    addEventHandler(element,$scope,attr,e);
                    if(e == 'click') {
                        element.className += ' bbq-clickable';
                    }
                }
            };
        });
    });
};

},{"../util":14}],6:[function(require,module,exports){
module.exports=[
    require('./repeat'),
    require('./events'),
    require('./model'),
    require('./show-hide'),
    require('./disabled'),
    require('./src'),
    require('./active'),
    require('./data'),
];

},{"./active":2,"./data":3,"./disabled":4,"./events":5,"./model":7,"./repeat":8,"./show-hide":9,"./src":10}],7:[function(require,module,exports){
var util = require('../util');

module.exports = function(bbq) {

    bbq.directive('model', function() {
        return {
            link: function(element, $scope, attr) {
                var l = function(ev) {
                    var setter = $scope.__lookupSetter__(attr);
                    setter.call($scope, ev.target.value, ev.target);
                };
                element.addEventListener('input', l, false);
                var parent = element.parentNode;
                while (parent && parent.nodeName != "BODY") {
                    if (parent.nodeName == "FORM") {
                        parent.addEventListener('reset', l, false);
                        break;
                    }
                    parent = parent.parentNode;
                }
                util.bindAttribute(element, 'value', $scope, attr);
            }
        };
    });
};

},{"../util":14}],8:[function(require,module,exports){
var Scope = require('../scope'),
    util = require('../util'),
    ObservableArray = require('../array');


function bfs(element, $scope) {
    this.processor(element, $scope);
    for (var i = 0; i < element.childNodes.length; i++)
        bfs.call(this, element.childNodes[i], $scope);
}

function createSubScope(template, parentObj, key, itemKey) {
    var $itemScope = new Scope(parentObj);
    $itemScope[itemKey] = parentObj[key];

    util.addWatcher(parentObj, key, function() {
        var setter = Object.getOwnPropertyDescriptor($itemScope, itemKey).set;
        if(util.isFunction(setter))
            setter.apply($itemScope, arguments);
    });
    var newItem = template.cloneNode(true);
    newItem.removeAttribute('bbq-repeat');
    bfs.call(this, newItem, $itemScope);
    return newItem;
}

module.exports = function(bbq) {
    bbq.directive('repeat', function() {
        return {
            link: function(element, $scope, attr) {
                var keys = attr.split(' in ');
                if (keys.length != 2)
                    throw new Error('Invalid bbq-repeat syntax');
                var itemKey = keys[0],
                    groupKey = keys[1];

                if(!$scope.lookup(groupKey))
                    return;

                if (!util.isArray($scope.lookup(groupKey)))
                    throw new Error('Cannot use non-array items with repeat (yet)');

                var anchor=document.createComment('End bbq-repeat');
                var template=element.cloneNode(true);
                element.parentNode.insertBefore(anchor,element);
                element.parentNode.removeChild(element);
                delete element;

                var collection = new ObservableArray();
                // TODO feels hacky. Clean way to do this?
                collection.__parent = $scope;
                collection.lookup = Scope.prototype.lookup;
                collection.set = Scope.prototype.set;
                collection.$watch = Scope.prototype.$watch;
                collection.__elements=[];
                collection.on('push',function(val,index){
                    var item=createSubScope.call(this, template, collection, index, itemKey);
                    anchor.parentNode.insertBefore(item, anchor);
                    collection.__elements.push(item);
                }.bind(this));

                collection.on('splice',function(start,howMany){
                    for(var i = 0; i < howMany; i++)
                    {
                        var element = collection.__elements[start+i];
                        anchor.parentNode.removeChild(element);
                    }
                    collection.__elements.splice(start,howMany);
                });

                collection.push.apply(collection,$scope.lookup(groupKey)); // add all

                $scope.set(groupKey,collection);

            }
        };
    });
};

},{"../array":1,"../scope":13,"../util":14}],9:[function(require,module,exports){
function show(element) {
    if (element.style.removeProperty)
        element.style.removeProperty('display');
    else
        element.style.display = '';
}

function hide(element) {
    element.style.display = 'none';
}

module.exports = function(bbq) {
    bbq.directive('show', function() {
        return {
            link: function(element, $scope, attr) {
                var up = function(val) {
                    if (!!val)
                        show(element);
                    else
                        hide(element);
                };
                up($scope.lookup(attr));
                $scope.$watch(attr, up);
            }
        };
    });
    bbq.directive('hide', function() {
        return {
            link: function(element, $scope, attr) {
                var down = function(val) {
                    if (!!val)
                        hide(element);
                    else
                        show(element);
                };
                down($scope.lookup(attr));
                $scope.$watch(attr, down);
            }
        };
    });
};

},{}],10:[function(require,module,exports){
module.exports = function(bbq)
{
    bbq.directive('src',function(){
        return {
            link: function(element, $scope, attr)
            {
                var src=$scope.lookup(attr);
                if(src)
                    element.setAttribute('src',src);
                $scope.$watch(attr,function(value){
                    if(value)
                        element.setAttribute('src',value);
                });
            }
        };
    });
};

},{}],11:[function(require,module,exports){
(function (global){
var bbq = require('./main');

// create global instance
global.bbq = new bbq();
require('./directives/').forEach(function(directive) {
    directive(global.bbq);
});

document.addEventListener('DOMContentLoaded', function() {
    if (window.bbq.DOMLoaded) // intentionally may throw reference error
        return;
    window.bbq.bootstrap();
});

}).call(this,typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./directives/":6,"./main":12}],12:[function(require,module,exports){
(function (global){
var util = require('./util'),
    Scope = require('./scope');

function processTextNode(el, $scope) {
    var str = el.textContent;
    var matches = str.match(/\{\{\s*([.\w^}])+\s*\}\}/g); /// find bracket tags
    if (!matches)
        return;
    var nodes = [];
    var unprocessed = str;
    // TODO use splitText instead
    for (var i = 0; i < matches.length; i++) {
        var tag = matches[i];
        var n = unprocessed.indexOf(tag);
        if (n > 0) {
            var t = document.createTextNode(unprocessed.substring(0, n));
            nodes.push(t);
        }
        var name = tag.replace(/[{}\s]/g, '');

        var span = document.createTextNode('');
        util.bindAttribute(span, 'textContent', $scope, name);
        nodes.push(span);

        unprocessed = unprocessed.slice(n + tag.length);
    }
    if (unprocessed)
        nodes.push(document.createTextNode(unprocessed));

    if (!nodes.length)
        return;
    var parentNode = el.parentNode;
    var lastNode = nodes[nodes.length - 1];
    parentNode.replaceChild(lastNode, el);
    for (var i = 0; i < nodes.length - 1; i++) {
        parentNode.insertBefore(nodes[i], lastNode);
    }
}

// Master class BBQ

function bbq() {
    this.__controllers = {};
    this.__directives = {};
    this.$rootScope = new Scope();
}

// Class Controller

bbq.prototype.controller = function(name, init) {
    var c = {
        name: name,
        $scope: new Scope(this.$rootScope)
    };
    c.$scope.$rootScope = this.$rootScope;
    init(c.$scope);
    this.__controllers[name] = c;
};

bbq.prototype.directive = function(key, processor) {
    var obj = processor();
    if (!util.isFunction(obj.link))
        throw new Error('Directive must have link function');
    this.__directives['bbq-' + key] = obj;
};

bbq.prototype.bootstrap = function() {
    global.document.removeEventListener('DOMContentLoaded', arguments.callee, false);


    this.visitChildren(global.document.body, null, this.processor.bind(this));

    this.DOMLoaded = true;
};

bbq.prototype.processor = function(element, $scope) {
    if (!$scope)
        return;

    if (element.getAttribute) {
        var added=[];
        for (var x in element.attributes) {
            var key = element.attributes[x].name;
            if (!this.__directives[key])
                continue;
            var value = element.getAttribute(key);
            this.__directives[key].link.call(this, element, $scope, value);
            added.push(key);
        }
        for(var x in added)
            element.removeAttribute(added[x]);
    }

    if (element.nodeType == Node.TEXT_NODE) {
        processTextNode(element, $scope); // text nodes bound to data
    }

};

bbq.prototype.visitChildren = function(element, controller, action) {
    if (!element.childNodes)
        return;
    if (element.getAttribute) {
        var att = element.getAttribute('bbq-controller');
        if (att && this.__controllers[att]) {
            var parent=false;
            if(controller && controller.$scope){
                parent = controller.$scope;
            }
            controller = this.__controllers[att];
            if(parent){
                controller.$scope.__parent = parent;
            }
            element.removeAttribute('bbq-controller');
        }
    }
    if (controller)
        action(element, controller.$scope);
    for (var i = 0; i < element.childNodes.length; i++) {
        this.visitChildren(element.childNodes[i], controller, action);
    }
};

module.exports = bbq;

}).call(this,typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./scope":13,"./util":14}],13:[function(require,module,exports){
var util = require('./util');

function Scope(parent) {
    if (!!parent)
        this.__parent = parent;
}

Scope.prototype.lookup = function(key) {
    var keyParts = (key.split) ? key.split('.') : [key];
    var obj = this;
    for (var i = 0; i < keyParts.length; i++) {
        obj = obj[keyParts[i]];
        if (!util.isDefined(obj))
            break;
    }
    if (util.isDefined(obj))
        return obj;
    else if (this.__parent && util.isFunction(this.__parent.lookup))
        return this.__parent.lookup(key);
    return undefined;
};

Scope.prototype.set = function(key, value) {
    var keyExists = typeof this.lookup(key) !== 'undefined';
    var keyParts = (key.split) ? key.split('.') : [key];
    var obj = this;
    var propName=keyParts[0];
    for (var i = 0; i < keyParts.length -1; i++) {
        if (!util.isDefined(obj[keyParts[i]])) {
            if(keyExists)
                break;
            obj[keyParts[i]]={};
        }
        obj = obj[keyParts[i]];
        propName = keyParts[i+1];
    }
    if(obj && (keyExists ? util.isDefined(obj[propName]) : true)){
        obj[propName]=value;
        return;
    }
    else if (this.__parent && util.isFunction(this.__parent.set))
        this.__parent.set(key, value);
    else
        throw new Error('Could not set value on scope');
};

Scope.prototype.$watch = function(key,update){
    return util.addWatcher(this,key,update);
};

module.exports = Scope;

},{"./util":14}],14:[function(require,module,exports){
var util = {};

util.parseEventCall = function(str,specials) {
    if (!str || !str.match)
        return null;
    var match = str.match(/^(\w+)\(([^)]*)\)/);
    if (!match)
        return null;
    var obj = {};
    obj.name = match[1];
    if(match.length < 3)
        obj.args=[];
    else
        obj.args = match[2].split(',').filter(function(s){
            return s.trim().length;
        }).map(function(arg){
            arg = arg.trim();
            var matches = arg.match(/^'([\w\s]+)'|"([\w\s]+)"|(-?\d+)|true|false$/); // strings and numbers and bool
            if(matches) {
                return {
                    type: 'constant',
                    value: eval(arg) // a very careful and deliberate use
                };
            }
            else if(util.isFunction(specials)) {
                var match = specials(arg);
                if(match)
                    return match;
            }
            return {
                type: 'variable',
                value: arg
            };
        });
    return obj;
};

util.isFunction = function(obj) {
    return !!obj && typeof obj === 'function';
};

util.isArray = function(obj) {
    return !!obj && obj instanceof Array;
};

util.isDefined = function(obj){
    return typeof obj !== 'undefined';
};

/**
 * Creates a get-set trigger on element for $scope,key.
 * If attr is set, this is the attribute to change on the element
 * @param  {Node} element DOM element
 * @param  {Scope} $scope  The scope to bind to
 * @param  {string} key     The item on the $scope to lookup
 * @param  {function} onUpdate    Action to be called on update
 * @return {void}
 */
util.addWatcher = function($scope, key, update) {
    if (util.isFunction($scope.lookup) && !$scope.lookup(key)) {
        $scope.set(key,'');
    }
    if (typeof $scope.__values === 'undefined') {
        $scope.__values = {};
    }
    if (typeof $scope.__watchers === 'undefined') {
        $scope.__watchers = {};
    }
    if (!$scope.__values[key]) {
        $scope.__values[key] = util.isFunction($scope.lookup) ? $scope.lookup(key) : $scope[key]; //TODO deep copy
    }
    $scope.__watchers[key] = $scope.__watchers[key] || [];
    $scope.__watchers[key].push(update);


    var obj=$scope;
    var keyParts = (key.split) ? key.split('.') : [key];
    var propName=keyParts[0];
    for (var i = 0; i < keyParts.length -1; i++) {
        if (!obj[keyParts[i]])
            break;
        obj = obj[keyParts[i]];
        propName=keyParts[i+1];
    }
    var descriptor = Object.getOwnPropertyDescriptor(obj, propName);
    if (descriptor && !descriptor.configurable) {
        return;
    }

    Object.defineProperty(obj, propName, {
        enumerable: true,
        configurable: false,
        set: function(val, trigger) {
            for (var x in $scope.__watchers[key])
                $scope.__watchers[key][x].call($scope, val, trigger);
            $scope.__values[key] = val;
        },
        get: function() {
            return $scope.__values[key];
        }
    });
};

util.bindAttribute = function(element, attribute, $scope, key) {
    util.setAttribute(element, attribute, $scope.lookup(key));
    util.addWatcher($scope, key, function(val, trigger) {
        util.setAttribute(element, attribute, val, trigger);
    });
};

util.setAttribute = function(element, attr, value, trigger) {
    if (!attr)
        throw new Error('attr undefined');
    if (element === trigger) {
        return;
    }
    element[attr] = value || '' ;
};

module.exports = util;

},{}]},{},[11])