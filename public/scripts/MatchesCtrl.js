bbq.controller('MatchesCtrl', function($scope){
	$scope.matches = [];
	$scope.date = new Date();
	$scope.cleanDate = '';

	var months = [ "January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December" ];
    var days = ["Sunday", "Moday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];    

    var getCleanDate = function(value) {
    	var text = days[value.getDay()] + ", ";
    	text += months[value.getMonth()] + " ";
    	text += value.getDate() + " at ";    	

    	if(value.getHours() == 0) {
    		text += "2:00 pm"
    	} else if(value.getHours() > 12) {
    		text += value.getHours() - 12;
    		text += ":" + value.getMinutes();

    		if(value.getMinutes() == 0)
    			text += "0"; 

    		text += " pm";
    	} else {
			text += value.getHours();
    		text += ":" + value.getMinutes();
			
			if(value.getMinutes() == 0)
    			text += "0";

    		text += " am";
    	}
    	
    	return text;
    };

    // Here is where I add the matches according to the clicked matches box
    $scope.$rootScope.$watch('selectedTime', function(value){
    	if(value != new Date()) {
        
	        console.log("selectedMatch changed!", value);   //should be a Date object
	        $scope.matches.splice(0);
	        
	        $scope.date = value;
	        // $scope.cleanDate = days[value.getDay()] + ", " + months[value.getMonth()] + " " + value.getDate() + " at " + value.getHours() + ":" + value.getMinutes() + "0";
	        $scope.cleanDate = getCleanDate(value);
	        
	    	var scheduleMatches = bbq.Schedule.getMatchesForTime(value);
	        
	        for(var match in scheduleMatches) {
	            $scope.matches.push(scheduleMatches[match]);
	        }
    	}
        
    });

    // Here is where I send the user to the person chat page with an invite selected
    $scope.setInvite = function(match,date) {
    	$scope.$rootScope.showPerson(match);
    };
});
