bbq.controller('ChatCtrl', function($scope) {
    $scope.messages = [];
    $scope.msgFocused = false;
    $scope.inviteDate = new Date();
    var selected = -1;
    var listBox;

    var dow=['Sun.','Mon.','Tues.','Wed.','Thur.','Fri.','Sat.'];

    $scope.dateToStr = function(d) {
        if(!d)
            return '';
        if(!d.getHours)
            d= new Date(d);
        if(!d)
            return '';
        var h = d.getHours();
        var dd = 'am';
        if (h >= 12) {
            dd = 'pm';
            h -= 12;
            h = h || 12;
        }
        return dow[d.getDay()] + ' ' + (d.getMonth() + 1) + '/' + d.getDate() + ', ' + h +' '+ dd;
    };
    function pushMessage(msg){
        $scope.messages.push(msg);
        $scope.scrollListBox();
    }

    $scope.msgFocus = function(event) {
        setTimeout($scope.scrollListBox, 350);
        $scope.msgFocused = true;
        $scope.set('headerShow',false);
    };
    $scope.msgBlur = function() {
        $scope.set('headerShow',true);
        $scope.msgFocused = false;
    };

    $scope.msgKeyup = function(event){
        if(event && event.keyCode == 13) // enter
        {
            $scope.sendMessage();
        }
    }

    $scope.scrollListBox = function($event) {
        listBox = listBox || document.getElementById('chatMessageList');
        listBox.scrollTop = listBox.scrollHeight;
    };

    $scope.$rootScope.$watch('selectedPerson', function(value) {
        if(value < 0 || value >= $scope.$rootScope.people.length){
            return;
        }
        selected = value;
        var p = $scope.$rootScope.people[value];
        $scope.name = p.name;
        $scope.thumbnail = p.thumbnail;
        $scope.messages.splice(0); // clears array
        p.messages.forEach(function(m) {
            pushMessage(m);
        });
        $scope.newMessage = '';
    });

    function handleError(msg) {
        console.error(msg);
    }
    var waiting = false;

    function getRandomResponse() {
        waiting = true;
        var xhr = new XMLHttpRequest();
        xhr.onload = (function(idx) {
            return function() {
                waiting = false;
                if (this.status >= 400) {
                    handleError("Could not get random message");
                } else {
                    var data = JSON.parse(this.responseText);
                    var msg = {
                        text: data.message,
                        from: false,
                        invite: false
                    };
                    var rand = Math.max(2, Math.random() * 45);
                    setTimeout(function() {
                        addMessage(msg, idx);
                    }, rand * 1000);

                    if(Math.random() > 0.9){
                    // only 10% of the time -- simulates randomness
                        setTimeout(function(){
                            getRandomResponse();
                        },10*1000);
                    }
                }
            };
        })(selected);
        xhr.open('GET', '/xhr/random_message', true);
        xhr.send();
    }

    function addMessage(msg, idx) {
        if(idx == selected)
            pushMessage(msg);
        if (idx >= 0 && $scope.$rootScope.people[idx]) {
            $scope.$rootScope.people[idx].messages.push(msg);
            if(selected != idx || $scope.$rootScope.currentView !== 'chat') {
                $scope.$rootScope.people[idx].new_messages++;
            }
            $scope.$rootScope.people[idx].messagePreview=msg.text;
        }

    }

    var sendBox;


    $scope.sendMessage = function($event) {
        if (!sendBox) {
            sendBox = document.getElementById('chatSendMessage');
        }
        if($event) $event.preventDefault();
        var msg = {
            text: $scope.newMessage,
            from: true,
            invite: false
        };
        addMessage(msg, selected);
        $scope.newMessage = '';
        if(!waiting)
            getRandomResponse();
        sendBox.focus();
    };

    $scope.toggleCalPicker = function() {
        $scope.calPickerOpen = !$scope.calPickerOpen;
        if ($scope.calPickerOpen) {
            $scope.scrollListBox();
        }
    };

    $scope.sendInvite = function(){
        $scope.calPickerOpen = false;
        addMessage({
            time: $scope.inviteDate,
            invite: true,
            pending: true,
            from: true
        }, selected);

    };

    //time is a Date object
    $scope.setInviteDate = function(time) {
        console.log("setInviteDate", time);

        $scope.inviteDate = time;
	}

	$scope.AcceptInvite = function(cell){
		cell.pending = false;
		cell.accepted = true;
	};

	$scope.RejectInvite = function(cell){
		cell.pending = false;
		cell.accepted = false;
	};

});
