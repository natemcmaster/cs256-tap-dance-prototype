(function(bbq){

	//////////////
	// CONSTANTS
	//////////////
	
	var startTime = 9;
	var endTime = 23;
	var daysToShow = 14;
	var dayLabels = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
	var today = new Date();


	/////////////////////
	// PUBLIC INTERFACE
	/////////////////////

	bbq.Schedule = {};

	bbq.Schedule.buildCalendar = function ($scope) {

		//Initialize
		$scope.days.splice(0);
		$scope.times.splice(0);
		$scope.cells.splice(0);

		//Build the days
		for (var daysOut = 0; daysOut < daysToShow; daysOut++) {
			var dateObject = new Date(today.getFullYear(), today.getMonth(), today.getDate() + daysOut);
			var day = {
				date: dateObject,
				dayText: dayLabels[dateObject.getDay()],
				dateText: (dateObject.getMonth()+1) + "/" + dateObject.getDate()
			};
			$scope.days.push(day);
		}

		//Build the times
		for (var hour = startTime; hour <= endTime; hour++) {
			var text = (hour == 12 ? "12PM" : (hour % 12));
			$scope.times.push(text);
		}

		//Build the cells
		for (var hour = startTime; hour <= endTime; hour++) {
			var row = [];
			for (var d = 0; d < $scope.days.length; d++) {
				var day = $scope.days[d];
				var cellDate = new Date(day.date.getFullYear(), day.date.getMonth(), day.date.getDate(), hour);
				row.push({
					date: cellDate,
					showEmpty: true,
					showMatches: false,
					showAppt: false,
					showAvailable: false,
					matches: 0
				});
			}
			$scope.cells.push(row);
		}

	};

	bbq.Schedule.addMatches = function ($scope) {
		for (var i = 0; i < $scope.$rootScope.schedule.length; i++) {
			//Get a time in the user's schedule
			var schedule = $scope.$rootScope.schedule[i];
			var cell = _getCell($scope, schedule.time);
			if (cell === null) continue;

			//Count the number of matches for that time
			var matches = bbq.Schedule.getMatchesForTime(schedule.time);
			if (matches.length > 0) {
				cell.showEmpty = false;
				cell.showMatches = true;
				cell.matches = matches.length;
			}
		}
	};

	bbq.Schedule.addPersonMatches = function ($scope, selectedPerson) {
		if (selectedPerson == -1) return;

		var person = $scope.$rootScope.people[selectedPerson];
		for (var i = 0; i < $scope.$rootScope.schedule.length; i++) {
			//Get a time in the user's schedule
			var schedule = $scope.$rootScope.schedule[i];
			var cell = _getCell($scope, schedule.time);
			if (cell === null) continue;

			if (bbq.Schedule.personHasMatch(person, schedule.time)) {
				cell.showEmpty = false;
				cell.showInactiveMatch = true;
			} else {
				// console.log("no match at " + schedule.time);
			}
		}
	};

	bbq.Schedule.getMatchesForTime = function (time) {
		
		var matches = [];
		
		//Count the number of matches for that time
		for (var p = 0; p < bbq.$rootScope.people.length; p++) {

			//We found a match.
			if (bbq.Schedule.personHasMatch(bbq.$rootScope.people[p], time)) {
				matches.push(
					bbq.$rootScope.people[p].name
				);
			}
		}
		
		return matches;
	};

	bbq.Schedule.personHasMatch = function (person, time) {
		for (var i = 0; i < person.schedule.length; i++) {
			var check = person.schedule[i].time;
			if (_timesEqual(check, time)) {
				return true;
			}
		}

		return false;
	};

	bbq.Schedule.addAppointments = function ($scope) {
		for (var i = 0; i < $scope.$rootScope.invites.length; i++) {
			var appt = $scope.$rootScope.invites[i];
			var cell = _getCell($scope, appt.time);
			if (cell === null) continue;

			cell.showEmpty = false;
			cell.showMatches = false;
			cell.showAppt = true;
			cell.appointment = appt;
		}
	};
	
	bbq.Schedule.addAvailables = function ($scope) {
		for (var i = 0; i < $scope.$rootScope.schedule.length; i++) {
			var appt = $scope.$rootScope.schedule[i];
			var cell = _getCell($scope, appt.time);
			if (cell === null) {
				console.error("Couldn't find cell");
			} else {
				cell.showEmpty = false;
				cell.showAvailable = true;
			}
		}
	};


	////////////////////
	// GETTERS/SETTERS
	///////////////////

	var _selectedTime = new Date();

	bbq.Schedule.setMyVar = function (newDate) {
	    _selectedTime = newDate;
	    bbq.$rootScope.selectedTime = newDate;
	};

	bbq.Schedule.getMyVar = function () {
	    return _selectedTime;
}


	////////////
	// HELPERS
	////////////

	function _getCell($scope, time) {

		// console.log("_getCell", time);

		var hour = time.getHours();

		var row = $scope.cells[hour - startTime];

		for (var i = 0; i < row.length; i++) {
			var cell = row[i];
			var cellDate = cell.date;
			if (_timesEqual(cellDate, time)) {
				return cell;
			}
		}

		return null;
	}

	function _datesEqual(dateA, dateB) {
		return (dateA.getFullYear() == dateB.getFullYear() &&
			dateA.getMonth() == dateB.getMonth() &&
			dateA.getDate() == dateB.getDate());
	}

	function _timesEqual(dateA, dateB) {
		return (_datesEqual(dateA, dateB) && dateA.getHours() == dateB.getHours());
	}


	/////////////////////
	// SCROLL LISTENERS
	/////////////////////

	document.addEventListener("DOMContentLoaded", function () {
		var panes = document.getElementsByClassName("schedule-pane");
		for (var i = 0; i < panes.length; i++) {
			addSchedulePaneScrollListener(panes[i]);
		}
	});

	function addSchedulePaneScrollListener(element) {
		element.addEventListener("scroll", function (event) {
			var scrollLeft = this.scrollLeft;
			var scrollTop = this.scrollTop;
			// console.log("scroll", scrollLeft, scrollTop);

			var days = this.getElementsByClassName("schedule-days")[0];
			days.style.left = (-1*scrollLeft) + "px";

			var times = this.getElementsByClassName("schedule-times")[0];
			times.style.top = (-1*scrollTop) + "px";
		});
	}

})(window.bbq);
