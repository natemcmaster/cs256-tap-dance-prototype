bbq.controller('ScheduleCtrl',function($scope){

	///////////////
	// INITIALIZE
	///////////////

	$scope.days = [];
	$scope.times = [];
	$scope.cells = [];

	$scope.$rootScope.$watchCalendar(function () {
		bbq.Schedule.buildCalendar($scope);
		bbq.Schedule.addMatches($scope);
		bbq.Schedule.addAppointments($scope);
	});

});
